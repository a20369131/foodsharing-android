package de.foodsharing.notifications

import android.content.Context
import com.google.gson.Gson
import de.foodsharing.api.PushSubscriptionAPI
import de.foodsharing.model.PublicKey
import de.foodsharing.model.PushSubscription
import de.foodsharing.services.PreferenceManager
import de.foodsharing.utils.ConnectivityReceiver
import de.foodsharing.utils.captureException
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PushService @Inject constructor(
    val context: Context,
    val preferenceManager: PreferenceManager,
    val pushSubscriptionAPI: PushSubscriptionAPI,
    val gson: Gson
) {
    var currentState: Boolean

    init {
        currentState = preferenceManager.pushNotificationsEnabled ?: false
        preferenceManager.observePushNotificationEnabled {
            if (!currentState && it == true) {
                enable()
            } else if (currentState && it != true) {
                disable()
            }
        }

        preferenceManager.observePushTokenOrPublicKeyUpdated {
            if (currentState) {
                enable()
            }
        }

        checkState()
    }

    private fun checkState() {
        // Check and try to fix the current state
        val subscription = preferenceManager.pushSubscription
        if (currentState && subscription == null) {
            enable()
        } else if (!currentState && subscription != null) {
            disable()
        }
    }

    private fun enable() {
        updateSubscription().observeOn(AndroidSchedulers.mainThread()).retryWhen {
            it.switchMap<Boolean> {
                it.printStackTrace()
                if (it is IOException) {
                    return@switchMap ConnectivityReceiver.observe()
                        .filter { it }.toFlowable(BackpressureStrategy.LATEST)
                }
                throw it
            }
        }.subscribe({
            currentState = true
        }, {
            it.printStackTrace()
            if (it is HttpException && (it.code() == 403 || it.code() == 401)) {
                // Not authorized, ignore
                return@subscribe
            }
            captureException(Exception("Unexpected exception while enabling push notifications.", it))
        })
    }

    private fun disable() {
        unsubscribe().observeOn(AndroidSchedulers.mainThread()).retryWhen {
            it.switchMap<Boolean> {
                it.printStackTrace()
                if (it is IOException) {
                    return@switchMap ConnectivityReceiver.observe()
                        .filter { it }.toFlowable(BackpressureStrategy.LATEST)
                }
                throw it
            }
        }.subscribe({
            currentState = false
        }, {
            it.printStackTrace()
            if (it is HttpException && (it.code() == 403 || it.code() == 401)) {
                // Not authorized, ignore
                return@subscribe
            }
            captureException(Exception("Unexpected exception while disabling push notifications.", it))
        })
    }

    fun updateSubscription(): Completable {
        val currentSubscriptionId = preferenceManager.pushSubscriptionId
        val currentSubscriptionString = preferenceManager.pushSubscription
        val currentSubscription = currentSubscriptionString?.let {
            gson.fromJson(it, PushSubscription::class.java)
        }
        val currentPublicKey = preferenceManager.pushPublicKey?.let {
            gson.fromJson(it, PublicKey::class.java)
        }
        val currentToken = preferenceManager.pushToken

        if (currentSubscriptionId != null || currentPublicKey == null || currentToken == null) {
            // Both values are not yet set, wait that both are initialized
            return Completable.complete()
        }

        val newSubscription = PushSubscription(
            currentPublicKey,
            currentToken
        )

        if (newSubscription == currentSubscription) {
            // No update necessary
            return Completable.complete()
        }

        return unsubscribe().toSingleDefault(true).flatMap {
            pushSubscriptionAPI.subscribe(newSubscription).doAfterSuccess {
                preferenceManager.pushSubscription = gson.toJson(newSubscription)
                preferenceManager.pushSubscriptionId = it.id
            }.subscribeOn(Schedulers.io())
        }.ignoreElement()
    }

    fun unsubscribe(): Completable {
        return preferenceManager.pushSubscriptionId?.let {
            pushSubscriptionAPI.unsubscribe(it).doOnComplete {
                preferenceManager.pushSubscription = null
                preferenceManager.pushSubscriptionId = null
            }.subscribeOn(Schedulers.io())
        } ?: Completable.complete()
    }
}
