package de.foodsharing.ui.base

import de.foodsharing.ui.base.BaseContract.Presenter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

abstract class BasePresenter<View> : Presenter<View> {

    protected var view: View? = null
    private val subscriptions = CompositeDisposable()

    fun <T> request(
        observable: Observable<T>,
        consumer: (T) -> Unit = {},
        onError: ((Throwable) -> Unit),
        autoDispose: Boolean = true
    ) {
        val subscription = observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer, {
                    onError(it)
                })
        if (autoDispose) {
            subscriptions.add(subscription)
        }
    }

    override fun unsubscribe() {
        subscriptions.clear()
    }

    override fun attach(view: View) {
        this.view = view
    }
}
