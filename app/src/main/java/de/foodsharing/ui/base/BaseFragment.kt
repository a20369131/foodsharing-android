package de.foodsharing.ui.base

import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.PreferenceManager
import de.foodsharing.utils.Utils
import javax.inject.Inject

abstract class BaseFragment : Fragment(), Injectable {

    @Inject
    protected lateinit var preferences: PreferenceManager

    protected fun showMessage(message: String, duration: Int = Snackbar.LENGTH_LONG) {
        activity?.let {
            Utils.createSnackBar(
                it.findViewById(android.R.id.content),
                message,
                ContextCompat.getColor(it, R.color.colorAccent),
                duration
            ).show()
        }
    }
}
