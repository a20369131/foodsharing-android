package de.foodsharing.ui.settings

import android.os.Bundle
import android.view.MenuItem
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.get
import de.foodsharing.BuildConfig
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_settings.toolbar
import javax.inject.Inject

class SettingsActivity : BaseActivity(), Injectable {

    @Inject
    lateinit var preferenceManager: PreferenceManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootLayoutID = R.id.settings_content
        setContentView(R.layout.activity_settings)

        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings_container, SettingsFragment())
            .commit()

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            if (BuildConfig.FLAVOR == "play") {
                // Show notification settings
                preferenceScreen.get<PreferenceCategory>(
                    requireContext().getString(R.string.preferenceCategoryPushNotifications))?.isVisible = true
            }
        }
    }
}
