package de.foodsharing.model

import java.io.Serializable
import java.util.Date

data class BasketRequest(
    val time: Date,
    val user: User
) : Serializable
